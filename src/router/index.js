import Vue from 'vue'
import Router from 'vue-router'
import login from '../views/login/index'
import home from '../views/home/index'
import register from '../views/register/index'
import scopes_list from '../views/gamecenter/scopes_list'
import api_usage_list from '../views/gamecenter/api_usage_list'
import api_changelog_list from '../views/gamecenter/api_changelog_list'
import center_list from '../views/gamecenter/center_list'
import change_email_list from '../views/gamecenter/change_email_list'
import change_password from '../views/gamecenter/change_password'

Vue.use(Router)
//解决路由跳转报错 Error: Navigation cancelled from "/login" to "/" with a new navigation
const originalPush = Router.prototype.push
Router.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/register',
      name: 'register',
      component: register
    },
    {
      path: '/gamecenter/scopes_list',
      name: 'scopes_list',
      component: scopes_list
    },
    {
      path: '/gamecenter/api_usage_list',
      name: 'api_usage_list',
      component: api_usage_list
    },
    {
      path: '/gamecenter/api_changelog_list',
      name: 'api_changelog_list',
      component: api_changelog_list
    },
    {
      path: '/gamecenter/center_list',
      name: 'center_list',
      component: center_list
    },
    {
      path: '/gamecenter/change_email_list',
      name: 'change_email_list',
      component: change_email_list
    },
    {
      path: '/gamecenter/change_password',
      name: 'change_password',
      component: change_password
    },
  ]
})
// 导航守卫
// 使用 router.beforeEach 注册一个全局前置守卫，判断用户是否登陆
// router.beforeEach((to, from, next) => {
//   if (to.path === '/login') {
//     next();
//   } else {
//     let token = localStorage.getItem('Authorization');
//
//     if (token === 'null' || token === '') {
//       console.log('eeee')
//
//       next('/login');
//     } else {
//       next();
//     }
//   }
// });

export default router
