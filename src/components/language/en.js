module.exports = {
  language: {
      name: '中文'
  },
  // 提示信息
  message: {
    username: 'Email',
    password:'Password',
    password1:'Password',
    code: 'checkcode',
    out: 'Sign Out',
    hint_win: 'Operation succeeded',

  },
  user: { // 登录
    username: 'Email address',
    username_hint:'Enter your email address',
    password:'Password',
    password_hint:'Enter your password',
    password_hint_is:'Forgot your password?',
    login:'Sign In',
    register:"Don't have an account？",
    register1:"Create an account",
    title: 'Sign in to Elements Data Dashboard',
    code: 'Code',
    code_hint: 'Please enter the code in the picture on the right',
    code_err: 'Code is invalid',
  },
  user_register: { // 注册
    username: 'Email address',
    username_hint:'Enter your email address',
    password_hint:'Enter your password',
    password_hint_is:'Confirm your password',
    login:'Creat account',
    cold: 'Verification Code',
    cold_hint: 'Enter verification code',
    subCold: 'Send Verification code',
    title1: 'Password',
    title2: 'Password confirmation:',
    title: 'Create an account',
    account: 'Already have an account? ',
    account1: 'Sign In',
    footer: 'By creating an account, you agree to the General Terms and Conditions of Sale and Privacy Policy',
    cold_message: 'The verification code will be sent to your email',
  },
  forgot_password: { //找回密码
    title: 'Reset your password',
    title_hint: 'Enter your email address and we will send you a verification code to reset your password',
    username_hint: 'Enter your email address',
    login:'Send password reset email',
  },
  check_your_mail: { //查看你的邮箱
    title: 'Check your Email',
    title_hint: 'The verification code has been sent to your email',
    subtitle_hint: 'If you haven\'t received an email within 5 minutes, check your spam folder or resend the email by pressing the button below',
    buttle: 'Resend Email',
    footer: 'For any questions or problems email us at',
    footer_emal: 'none_sayno@163.com',
    cold: 'Verification Code',
    cold_hint: 'Enter verification code',
    subCold: 'Send Verification code',
    title1: 'Password',
    title2: 'Password confirmation',
    password_hint: 'Enter your new password',
    new_password_hint: 'Confirm your new password',
    button: 'Update Password'
  },
  // 首页
  nav: {
    token: 'Access Token',
    scopes: 'Access Scopes',
    usage: 'Data Usage',
    changelog: 'API Changelog',
    center: 'Account',
    authentication:  'Profile',
    mailbox_setting: 'Mailbox Setting',
    password_setting: 'Account security',
  },
  // 密匙管理
  key_title: {
    title: 'Your access token',
    title_hint: {
      subtitle: 'Here is your token. You need to add it as a parameter to every API queries you make.',
      buttle: 'Get started',
      subtitle1: ' to know how to use your token with the Elements Data API.'
    },
    title_list: {
      title: 'Manage your access tokens',
      token: 'Token',
      state: 'State',
      option: 'Action',
      active: 'Active',
      disable: 'Inactive',
      butStop: 'Disable',
      butCreate: 'Reset',
      butRevoke: 'Reset',
      bueEnable: 'Enable',
      hint_begin: 'Are you sure you want to enable your token?',
      hint_stope: 'Are you sure you want to disable your token?',
      hint_reset: 'Are you sure you want to reset your token?',
      hint_win: 'Operation succeeded',
      but_yse: 'YES',
      but_no: 'NO',
      hint: 'Hint',
      hint_copy: 'Copied'
    }
  },
  // 认证信息——公共
  perfect_info: {
    title: 'Edit Profile',
    nickname: 'Display Name',
    email: 'Email',
    resetemail: 'Change Login Email？',
    mobile: 'Mobile',
    phone: 'Phone',
    country: 'Country',
    time_zone: 'Time Zone',
    but: 'Update Profile',
    hint_win: 'Operation succeeded',
  },
  // 认证信息-个人
  message_title: {
    title: 'Your Email:',
    title_hint: 'The following information will be used for issuing invoices, please fill in with caution.',
    Identity_type: {
      type: 'Identity Registration',
      Personal: 'Personal',
      Company: 'Company'
    },
    first_name: 'First Name',
    last_name: 'Last Name',
    country: 'Country',
    address: 'Address Line',
    city: 'City',
    region: 'Country/Region',
    footer: 'I hereby confirm that the information above, including information provided at the time of registration of this ELS account, is complete, truthful and accurate, and will promptly provide ELS with written notice of any updates thereto. I consent to the collection, use, storage and disclosure of this information for the purposes of risk control and compliance with applicable law. I understand and consent to the transmission of this information to ELS for these purposes.',
    button: 'Submit'
  },
  // 认证信息-企业
  company_message_title: {
    company_name: 'Company Name',
    company_address: 'Address Line',
    city: 'City',
    info_title: 'Contact Information',
    first_name: 'First Name',
    last_name: 'Last Name',
    email:'Email',
    footer: 'I hereby confirm that the information above, including information provided at the time of registration of this ELS account, is complete, truthful and accurate, and will promptly provide ELS with written notice of any updates thereto. I consent to the collection, use, storage and disclosure of this information for the purposes of risk control and compliance with applicable law. I understand and consent to the transmission of this information to ELS for these purposes.',
    button: 'Submit'
  },
  // 置换邮箱
  change_email_title: {
    title_hint: 'We need to verify your password for the next step',
    username: 'Change Login Email',
    password_hint: 'Enter your password',
    subtitle: 'Check your Email',
    subtitle_hint: 'The verification code has been sent to your email',
    cold: 'Verification Code',
    code_hint: 'Enter verification code',
    newEmail: 'Email address',
    newEmail_hint: 'Enter your new email address',
    footer: '如果您在5分钟内仍未收到电子邮件，请检查您的垃圾邮件文件夹或按下方按钮重新发送。',
    button: 'Send Verification code',
    button1: 'Save your modifications',
    but_next: 'Next',
    but_update: 'Update Login Email'
  },
  // 修改密码
  change_password: {
    cccount: 'Account',
    cccount_hint: 'Please complete the following information so that we can serve you better',
    nickname: 'Nickname',
    email: 'Email',
    resetemail: 'Reset email？',
    title: 'Change password',
    title_hint: 'Please complete the following information to change your current password',
    old_password: 'Old password',
    old_password_hint: 'Enter your old password',
    new_password_hint: 'Confirm your new password',
    password_hint: 'Enter your new password',
    new_password: 'New password',
    new_password_conf: 'Confirm new password',
    button: 'Update Password',
    cancel: '取消'
  },
  // 数据权限
  scopes: {
    token: 'Please contact sales@elementsdata.cn to Get free trial',
    plan: 'Plan',
    free: 'Free',
    basic: 'Basic',
    professional: 'Professional',
    primary: 'Primary',
    games: 'Games',
    type: 'Your Plan',
    time: 'Expiration Time'
  },
  // 数据用途
  usages: {
    name_api: 'API Usage',
    duration: 'In last hour',
    name_data: 'Data Usage',
    time: 'Time Period',
    hour: '1 Hour',
    hour3: '3 Hour',
    hour12: '12 Hour',
    hour24: '24 Hour',
    day3: '3 Day',
    day7: '7 Day',
    total: 'Request Statistics',
    total_name: 'Request Statistics',
    response: 'Bytes in Response',
    delay1: 'P90 Latency',
    delay: 'P99 Latency',
    table_use: {
      request: 'Endpoint',
      endpoint: 'Request Count',
      delay: 'Latency(MS)',
      p50: 'P50',
      p90: 'P90',
      p99: 'P99',
    },
    table_log: {
      title_hint: 'Request Log(Last 3 Day)',
      status: 'Code',
      request: 'Endpoint',
      params: 'Parameters',
      response: 'Response Time(MS)',
      date: 'Request Date',
      code: 'Code'
    },
    table_err: {
      title_hint: 'Error Requests',
      request: 'Endpoint',
      count: 'Request Count',
      response: 'Latency(MS)',
      code: 'Code'
    }
  }
}

