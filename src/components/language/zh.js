module.exports = {
    language: {
        name: 'English'
    },
  // 提示信息
  message: {
    username: '邮箱',
    password:'密码',
    password1:'确认密码',
    code: '验证码',
    out: '登出',
    hint_win: '操作成功',

  },
  // 登录、注册
    user: {
      username: '邮箱',
      username_hint:'输入您的邮箱',
      password:'密码',
      password_hint:'输入您的密码',
      password_hint_is:'忘记密码?',
      login:'登录',
      register:'没有账号？',
      register1:'创建账号',
      title: '登录Elements Data控制台',
      code: '验证码',
      code_hint: '输入右侧的验证码',
      code_err: '验证码错误',
    },
  user_register: {
    username: '邮箱',
    username_hint:'输入您的邮箱',
    password_hint:'输入您的密码',
    password_hint_is:'确认密码',
    login:'创建账号',
    cold: '验证码',
    cold_hint: '输入验证码',
    subCold: '发送验证码',
    title1: '密码',
    title2: '确认密码',
    title: '创建账号',
    account: '已有账号？',
    account1: '马上登录',
    footer: '创建账户即视为您认可销售与隐私政策的基本条款和条件',
    cold_message: '验证码将发送至您的邮箱',
  },
  forgot_password: {
    title: '重置您的密码',
    title_hint: '输入您的邮箱地址，我们将给您发送重置密码的验证码',
    username_hint: '输入您的邮箱',
    login:'发送重置密码邮件',
  },
  check_your_mail: {
    title: '检查您的邮箱',
    title_hint: '验证码已经发送至您的邮箱',
    subtitle_hint: '如果您在5分钟内仍未收到电子邮件，请检查您的垃圾邮件文件夹或按下方按钮重新发送。',
    buttle: '重新发送',
    footer: '如有任何疑问或问题，请联系我们',
    footer_emal: 'none_sayno@163.com',
    cold: '验证码',
    cold_hint: '输入验证码',
    subCold: '发送验证码',
    title1: '密码',
    title2: '确认密码',
    password_hint: '输入您的新密码',
    new_password_hint: '再次输入您的新密码',
    button: '更新密码'
  },
  // 首页
  nav: {
      token: '密匙管理',
      scopes: '数据权限',
      usage: '数据用途',
      changelog: 'API变更录',
      center: '个人中心',
    authentication:  '账号信息',
    mailbox_setting: '更换邮箱',
    password_setting: '账号安全',
  },
  // 密匙管理
  key_title: {
      title: '您的 Token',
      title_hint: {
        subtitle: '这是您的密钥，您需要将其作为参数添加至所需的每个API查询中，点 ',
        buttle: '这里',
        subtitle1: ' 了解如何在Elements Data API中使用您的密钥。'
      },
    title_list: {
        title: '管理您的密钥',
        token: '密钥',
        state: '状态',
        option: '操作',
        active: '活跃',
        disable: '不活跃',
        butStop: '停用',
        butRevoke: '重新生成',
        butCreate: '生成',
        bueEnable: '启用',
       hint_begin: '确定要启用您的密钥吗？',
       hint_stope: '确定要停用您的密钥吗？',
       hint_reset: '确定要重置您的密钥吗？',
       hint_win: '操作成功',
      but_yse: '是',
      but_no: '否',
      hint: '提示',
      hint_copy: '复制成功'
    }
  },
  // 认证信息——公共
  perfect_info: {
    title: '修改账号信息',
    nickname: '显示名称',
    email: '邮箱',
    resetemail: '修改登录邮箱？',
    mobile: '移动电话',
    phone: '固定电话',
    country: '国家',
    time_zone: '时区',
    but: '更新账号信息',
    hint_win: '操作成功',
  },
  // 认证信息-个人
  message_title: {
      title: '您当前账户:',
    title_hint: '以下信息将用于开具发票，请谨慎填写。',
    Identity_type: {
        type: '选择身份类型',
      Personal: '个人',
      Company: '企业'
    },
    first_name: '名',
    last_name: '姓',
    country: '国家',
    address: '省份/直辖市',
    city: '城市',
    region: '详细地址',
    footer: '我确认上述提供的信息，包括在ELS帐号注册时提供的信息,都是完整、真实及准确的，并且我将及时书面通知ELS如果上述信息有任何更新。我同意以风控和符合相关法规要求的目的收集、使用、存储、披露上述信息。我理解并同意为实现上述目的将这些信息传输至ELS。',
    button: '提交'
  },
  // 认证信息-企业
  company_message_title: {
    company_name: '企业名',
    company_address: '公司地址',
    city: '城市',
    info_title: '企业法人信息',
    first_name: '名',
    last_name: '姓',
    email:'邮箱',
    footer: '我确认上述提供的信息，包括在ELS帐号注册时提供的信息,都是完整、真实及准确的，并且我将及时书面通知ELS如果上述信息有任何更新。我同意以风控和符合相关法规要求的目的收集、使用、存储、披露上述信息。我理解并同意为实现上述目的将这些信息传输至ELS。',
    button: '提交'
  },
  // 置换邮箱
  change_email_title: {
    title_hint: '我们需要验证您的密码来进行下一步',
    username: '修改登录邮箱',
    password_hint: '输入登录密码',
    subtitle: '检查您的邮箱',
    subtitle_hint: '验证码已经发送至您的邮箱',
    cold: '验证码',
    code_hint: '输入验证码',
    newEmail: '邮箱',
    newEmail_hint: '输入您的新邮箱',
    footer: '如果您在5分钟内仍未收到电子邮件，请检查您的垃圾邮件文件夹或按下方按钮重新发送。',
    button: '发送验证码',
    button1: '重新邮件',
    but_next: '下一步',
    but_update: '更新登录邮箱'
  },
  // 修改密码
  change_password: {
    cccount: '完善您的账户',
    cccount_hint: '请完善以下信息，以便我们更好的为您服务',
    nickname: '昵称',
    email: '邮箱',
    resetemail: '重置邮箱？',
    title: '修改账号密码',
    title_hint: '请完善以下信息，用以修改你的当前密码',
    old_password: '老密码',
    old_password_hint: '输入您的老密码',
    password_hint: '输入您的新密码',
    new_password_hint: '再次输入您的新密码',
    new_password: '新密码',
    new_password_conf: '确认新密码',
    button: '更新密码',
    cancel: '取消'
  },
  // 数据权限
  scopes: {
    token: '请联系sales@elementsdata.cn，获取免费试用权限',
    plan: '套餐',
    free: '免费',
    basic: '基础',
    professional: '专业',
    primary: '内部',
    games: '游戏',
    type: '您的套餐',
    time: '到期时间'
  },
  // 数据用途
  usages: {
    name_api: 'API 使用概况',
    duration: '最近1小时',
    name_data: '数据使用',
    time: '统计周期',
    hour: '1小时',
    hour3: '3小时',
    hour12: '12小时',
    hour24: '24小时',
    day3: '3天',
    day7: '7天',
    total: '请求总数',
    total_name: '请求统计',
    response: '返回字节',
    delay1: 'P90 延迟',
    delay: 'P99 延迟',
    prve: '上页',
    next: '下页',
    table_use: {
      request: '端口',
      endpoint: '请求总数',
      delay: '平均延迟（毫秒）',
      p50: 'P50',
      p90: 'P90',
      p99: 'P99',
    },
    table_log: {
      title_hint: '请求日志（最近3天）',
      status: '状态码',
      request: '端口',
      params: '参数',
      response: '响应时间（毫秒）',
      date: '请求日期',
      code: '状态码'
    },
    table_err: {
      title_hint: '请求报错',
      request: '端口',
      count: '请求总数',
      response: '平均延迟（毫秒）',
      code: '状态码'
    }
  }
}
