import Vue from 'vue'
import Vuex from 'vuex'
import user from "./modules/user";
Vue.use(Vuex)


const store = new Vuex.Store({
  modules: {
    user
  }

//   state: {
//     // 存储token
//     Authorization: localStorage.getItem('Authorization') ? localStorage.getItem('Authorization'): ''
//   },
//
//   mutations: {
// // 修改token，并将token存入localStorage
//     changeLogin (state, user) {
//       state.Authorization = user.Authorization
//       localStorage.setItem('Authorization', user.Authorization)
//     }
//   }
})

export default store
