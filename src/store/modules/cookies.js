import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'

// 在Cookie中取出
export function getToken() {
  return Cookies.get(TokenKey)
}

// 存到Cookie去
export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

// 删除
export function removeToken() {
  return Cookies.remove(TokenKey)
}
