import { login, getInfo, logout } from "../../api/user"; // 引入登录接口
import { getToken, setToken, removeToken } from '../modules/cookies'
const state= {
  token: getToken(),
  email: '',
  lang: '',
  user: {}
}

const mutations = { //操作 用于更新state
  SET_TOKEN: (state, token) => {
    state.token = token
    setToken(token)
  },
  SET_EMAIL: (state, email) => {
    state.email = email
  },
  USER: (state, user) => {
    state.user = user
  },
  SET_LANG: (state, lang) => {
    state.lang = lang
  }
}
const actions = { //方法
  getLang({commit, state}) {
    const { lang } = state
    commit('SET_LANG', lang)
  },
  login({ commit }, userInfo) {
    const { username, password} = userInfo
    return new Promise((resolve, reject) => {
      login({ email: username.trim(), password: password }).then(response => {
        const { data } = response // 解构出data

        commit('SET_TOKEN', data.token) // 更新store里面的token
        setToken(data.token) // token保存到cookie
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      // 调用getInfo接口
      // const params = {}
      // params['token'] = state.token
      // console.log('传参数',params)
      getInfo().then(response => {
        const { data } = response // 解构出data
        if (!data) { // 进行数据校验
          reject('没有返回值')
        }
        // 解构出需要保存的值
        const { email } = data
        // 保存到vuex
        commit('SET_EMAIL', email)
        commit('USER', data)
        // 将 data 返回
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },
  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        commit('SET_TOKEN', '')
        commit('SET_EMAIL', '')
        removeToken()

        // reset visited views and cached views
        // to fixed https://github.com/PanJiaChen/vue-element-admin/issues/2485
        // dispatch('tagsView/delAllViews', null, { root: true })

        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },
  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      commit('SET_EMAIL', '')
      removeToken()
      resolve()
    })
  }
}
export default {
  namespaced: true,
  state,
  actions,
  mutations
}
