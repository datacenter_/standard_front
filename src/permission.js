import router from "./router";
import store from "./store";
import { getToken } from './store/modules/cookies'


const whiteList = ['/login', '/register'] // no redirect whitelist
// 路由拦截器
router.beforeEach(async (to, from, next) => {
  // 从cookie中取得token
  const hashToken = getToken()
  // 如果有token 也就是已经登陆的情况下
  if (hashToken) {
    console.log('跳转到哪里',to.path)
    if (to.path === '/login') {
      // 并且要前往的路径是\\'/login\\'  则返回 \\'/\\'
      next({path: '/'})
    }else {
      // 从store中取得用户的 roles, 也就是用户的权限 并且用户的权限数组必须有一个以上
      const hasRoles = store.state.user.email.length > 0
      // 有权限则直接进入
      if (hasRoles) {
        next()
      } else {
        // 没有权限的话
        try {
          // 获取用户信息
          const { email } = await store.dispatch('user/getInfo')
          // 生成可访问路由
          // const accessRoutes = await store.dispatch('permission/generateRoutes', roles)
          // 将可访问路由添加到路由上
          // router.addRoutes(accessRoutes)
          // 进入路由
          next()
        } catch (error) {
          console.log('出现异常',error)
          // 如果出现异常  清空路由
          await store.dispatch('user/resetToken')
          // Message提示错误
          // Message.error(error || 'Has Error')
          // 跳到login页面重新登陆
          next(`/login?redirect=${to.path}`)
        }
      }
    }
  }else {
    console.log('继续呀')
    // 没有token 也就是没有登陆的情况下
    // 判断是否是白名单(也就是说不需要登陆就可以访问的路由)
    if (whiteList.indexOf(to.path) !== -1) {
      next()
    } else {
      // 其他的一路给我跳到login页面 老老实实的进行登陆
      next(`/login?redirect=${to.path}`)
      // NProgress.done()
    }
  }
})
