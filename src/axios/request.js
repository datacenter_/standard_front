// 统一处理接口请求返回正确错误作出相应判断
import axios from 'axios'
import baseURL from '../../config/api'
import store from "../store";
import Vue from 'vue'
//报警弹窗提示
import ToastPlugin from 'bootstrap-vue'
Vue.use(ToastPlugin)
let vm = new Vue()
// 创建axios实例
const service = axios.create({
  baseURL: baseURL.Base_store_URL,
  timeout: 5000 // 请求超时时间
})

// request拦截器 对请求参数做处理  请求的时候操作
service.interceptors.request.use(config => {
  //判断是否存在token，如果存在的话，则每个http header都加上token
  if (store.state.user.token) {
    config.headers['X-Access-Token'] = store.state.user.token
  }
  return config;
},
  error => {

    // do something with request error
    console.log('点点滴滴',error) // for debug
    return Promise.reject(error)
  }
)

// /****** respone拦截器==>对响应做处理 ******/ 请求回来的参数
service.interceptors.response.use(
  response => {  //成功请求到数据
    const res = response.data

    // if the custom code is not 20000, it is judged as an error.定义的是返回code = 200是正常
    if (res.code !== 200) {
      if (res.code !== 10400) {
        vm.$bvToast.toast(res.message, {
          noCloseButton: true,
          variant: 'danger',
          toaster: 'b-toaster-top-center',
          solid: true
        })
      }
      return Promise.reject(new Error(res.message || 'Error'))
    } else {
      return res
    }
  },
  error => {
    console.log('err' + error) // for debug
    vm.$bvToast.toast(error.message, {
      noCloseButton: true,
      variant: 'danger',
      toaster: 'b-toaster-top-center',
      solid: true
    })
    return Promise.reject(error)
  }
)

export default service
