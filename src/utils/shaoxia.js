const shaoxia = {
// JS取深度对象中的值
  safeProps(fn, defaultVal) {
    try {
      return fn();
    } catch (e) {
      return defaultVal;
    }
  },
}
export default shaoxia
