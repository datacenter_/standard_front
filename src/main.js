// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import {BootstrapVue, IconsPlugin} from 'bootstrap-vue'
import './utils/index.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import store from './store'
import './permission.js'
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.config.productionTip = true

import shaoxia from './utils/shaoxia'
Vue.prototype.$shaoxia = shaoxia

import VDistpicker from 'v-distpicker'
Vue.component('v-distpicker', VDistpicker);

import VueBootstrapToasts from "vue-bootstrap-toasts";
Vue.use(VueBootstrapToasts);


//国际化中英文切换
import VueI18n from 'vue-i18n'
Vue.use(VueI18n)
const i18n=new VueI18n({
  locale:localStorage.getItem('languageSet')||'zh',   //从localStorage里获取用户中英文选择，没有则默认中文
  messages:{
    'zh':require('./components/language/zh'),
    'en':require('./components/language/en')
  }
})



// 表单验证
// 您必须配置vee-validate的 fields属性，否则它在注入自身时将与<b-table>的： :fieldss属性（可能还有其他组件）冲突。
import VeeValidate, {Validator} from "vee-validate";
import zh from 'vee-validate/dist/locale/zh_CN'
import en from 'vee-validate/dist/locale/en'
// 初始化表单验证，同时使用i18n
Vue.use(VeeValidate,{
  i18n,
  i18nRootKey: 'validation',
  dictionary: {
    zh,
    en
  },
  // This is the default
  inject: true,
  // Important to name this something other than 'fields'
  fieldsBagName: 'veeFields',
  // This is not required but avoids possible naming conflicts
  errorBagName: 'veeErrors'
})
Validator.localize('zh', {
  messages: zh.messages,
})
Validator.localize('en', {
  messages: en.messages,
})
export function setMessage(validName, errMsgZh, errMsgEn, validate) {
  console.log('语言', Validator.locale)
  if (Validator.locale == 'en') {
    // 自定义验证规则,取名为validName, 通过该方式使用v-validate="'required|phone'"   v-validate.continues="'phoneNum'"实现必填
    Validator.extend(validName, {
      // 提示信息，不符合规则提示语
      getMessage: (field, [args]) => {
        return errMsgEn
      },
      // 验证规则，符合规则通过，否则不通过 (规则为美国电话号码)
      validate: validate
    })
  }
  if (Validator.locale == 'zh') {
    Validator.extend(validName, {
      getMessage: (field, [args]) => {
        return errMsgZh
      },
      validate: validate
    })
  }

}
// 修改默认错误提示
const dict = {
  zh: {messages: {email: (name) => `${name}不合法`}},
  en: {messages: {email: (name) => `${name} is invalid`}},
}
Validator.localize(dict)
const dict1 = {
  zh: {messages: {required: (name) => `必须填写`}},
  en: {messages: {required: (name) => `Field required`}},
}
Validator.localize(dict1)
const dict2 = {
  zh: {messages: {confirmed: (name) => `两次输入的密码不一致`}},
  en: {messages: {confirmed: (name) => `Password doesn't match`}},
}
Validator.localize(dict2)
// 添加规则
// 注册提示
setMessage('requireds', '必须填写', 'Field required', (value, [args]) => {
  const reg = /\S/;
  return reg.test(value)
})
setMessage('y_password', '密码必须为8-16位字母和数字组合', 'The password must be a combination of 8-16 letters and numbers', (value, [args]) => {
  const reg = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,16}$/;
  return reg.test(value)
})
setMessage('y_tmp', '电话格式错误', 'Telephone format error', (value, [args]) => {
  const reg = /[\d]([-])*$/;
  return reg.test(value)
})
// 登录提示

// setMessage('phoneNum1', '手机号不正确', 'phoneNum error', (value, [args]) => {
//   const reg = /^((13|14|15|17|18)[0-9]{1}\d{8})$/
//   return reg.test(value)
// })
// setMessage('personName', '姓名只能包括中文字或英文字母', 'username no yes', (value, [args]) => {
//   const reg = /^([\u4e00-\u9fa5\s]|[a-zA-Z])*$/
//   return reg.test(value)
// })





/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  i18n,   //把 i18n 挂载到 vue 根实例上
  render: h => h(App),
  components: { App },
  template: '<App/>'
})
